def esPositivo(x):
    if x>0:
        return True
    return False
def esPositivoAlt(x):
    if x>=0:
        return True
    return False

def registrarProductos():
    producto = []
    bucle = "si"
    agregar = str(input("Descripcion del producto a agregar."))
    producto.append(agregar)
    while bucle == "si": 
        agregar = float(input("Precio del producto."))
        if (esPositivo(agregar)):
            producto.append(agregar)
            bucle = "no"
        else:
            print("Introduzca un valor mayor a cero.")    
            bucle = "si"
    bucle = "si"
    while bucle == "si": 
        agregar = int(input("Stock del producto."))
        if (esPositivoAlt(agregar)):
            producto.append(agregar)
            bucle = "no"
        else:
            print("Introduzca un valor mayor o igual a cero.")    
            bucle = "si"
    return producto

def registroCompleto(diccionarioProductos):
    terminar = "no"
    listaClaves = list(diccionarioProductos.keys())
    clave = int(input("Introduzca un codigo para el producto."))
    if diccionarioProductos != {}:
        while terminar == "no":
            for i in listaClaves:
                if i == clave:
                    print("Este codigo ya esta asignado a un producto") 
                    return diccionarioProductos
                else:
                    diccionarioProductos[clave] = registrarProductos()
                    terminar = "si"
                    break
    else:
        diccionarioProductos[clave] = registrarProductos()

    return diccionarioProductos


def mostrarProductos(diccionarioProductos):
    listaClaves = list(diccionarioProductos.keys()) #es una lista
    listaValores = list(diccionarioProductos.values()) #es una lista
    for i in range(len(listaClaves)):
        print("CODIGO:",listaClaves[i], "PRODUCTO:", listaValores[i][0], "PRECIO: $", listaValores[i][1], "STOCK:",listaValores[i][2]) 

def productosDesdeHasta(diccionarioProductos,desde,hasta):
    listaClaves = list(diccionarioProductos.keys()) #es una lista
    listaValores = list(diccionarioProductos.values()) #es una lista
    diccionarioLocal = {}
    print("Los siguientes productos estan en el rango de stock seleccionado:")
    for i in range(len(listaClaves)):
        if (hasta >= listaValores[i][2] >= desde):
            diccionarioLocal[listaClaves[i]] = listaValores[i]    
    mostrarProductos(diccionarioLocal)

def reArmarDiccionario(listaClaves,listaValores):

    nuevoDiccionario = {}
    for i  in range(len(listaClaves)):
        nuevoDiccionario[listaClaves[i]] = listaValores[i]

    return nuevoDiccionario

def dadoStockYSumarX(diccionarioProductos,Y,X): #Si es menor que Y >>> Suma X
    listaClaves = list(diccionarioProductos.keys()) #es una lista
    listaValores = list(diccionarioProductos.values()) #es una lista
    for i in range(len(listaClaves)):
        if (listaValores[i][2] < Y): # 4 < 5
            listaValores[i][2] += X # listaValores[0][2] = listaValores[0][2]+Agregar
    return reArmarDiccionario(listaClaves, listaValores)

def eliminarStockCero(diccionarioProductos):
    listaClaves = list(diccionarioProductos.keys()) #es una lista
    listaValores = list(diccionarioProductos.values()) #es una lista
    clavesDeProductosBorrar = []
    for i in range(len(listaClaves)):
        if (listaValores[i][2] == 0):
            clavesDeProductosBorrar.append(listaClaves[i])
    
    for i in clavesDeProductosBorrar:
        del diccionarioProductos[i]

    return diccionarioProductos    

def menu():
    print("a - Registrar productos")
    print("b - Mostrar productos cargados")
    print("c - Mostrar productos en un intervalo de stock")
    print("d - Sumar X stock a un producto con stock < Y")
    print("e - Eliminar productos con stock 0")
    print("f - Salir")
    decision = str(input("Seleccione una opcion."))
    return decision

productos = {}
salir = "no"
while salir != "si":
   opcion =  menu()
   if opcion == 'a':
       productos = registroCompleto(productos)
   elif opcion == 'b':
       mostrarProductos(productos)
   elif opcion == 'c':
        intervaloInferior = int(input("Intervalo inferior"))
        intervalorSuperior = int(input("Intervalo superior"))
        if esPositivo(intervaloInferior) and esPositivo(intervalorSuperior):
            productosDesdeHasta(productos,intervaloInferior,intervalorSuperior)
        else:
            print("Uno o mas valores del intervalo son negativos.")
   elif opcion == 'd':
        sumar = float(input("Cuanto stock X desea sumar?"))
        menorQue = float(input("A productos que tengan stock Y menor que..."))
        if esPositivo(sumar) and esPositivo(menorQue):
            productos = dadoStockYSumarX(productos, menorQue, sumar)
        else:
            print("Uno o mas valores que ha introducido son negativos.")
   elif opcion == 'e':
        productos = eliminarStockCero(productos)
   elif opcion == 'f':
        salir = "si"
        print("Gracias por usar el programa.")
   else:
       print("Introduzca una opcion correcta")
